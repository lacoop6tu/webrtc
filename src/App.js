import React, { useState, useEffect, useRef } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles'
import ImageUploader from "react-images-upload"
import TextField from '@material-ui/core/TextField'
import Autocomplete from '@material-ui/lab/Autocomplete'
import Checkbox from '@material-ui/core/Checkbox'
import CheckBoxIcon from '@material-ui/icons/CheckBox'
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank'
// import Button from '@material-ui/core/Button'
// import SaveIcon from '@material-ui/icons/Save'
import Fab from '@material-ui/core/Fab'
import TrendingFlat from '@material-ui/icons/TrendingFlat'
import AddIcon from '@material-ui/icons/Add'
import Snackbar from '@material-ui/core/Snackbar'
import CloseIcon from '@material-ui/icons/Close'
import IconButton from '@material-ui/core/IconButton'
import Gallery from 'react-grid-gallery'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPhone } from '@fortawesome/free-solid-svg-icons'
import { faVideo } from '@fortawesome/free-solid-svg-icons'
import _ from 'lodash'
import { useDispatch, useSelector } from 'react-redux'
import Sidebar from "react-sidebar"
import Button from '@material-ui/core/Button'
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles'
import axios from 'axios'
import Divider from '@material-ui/core/Divider'
import Modal from '@material-ui/core/Modal'
import Backdrop from '@material-ui/core/Backdrop'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import Avatar from '@material-ui/core/Avatar'
import ImageIcon from '@material-ui/icons/Image'
import WorkIcon from '@material-ui/icons/Work'
import BeachAccessIcon from '@material-ui/icons/BeachAccess'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'

import { server_url, enter_path, getonlineusers_path, createroom_path, enterroom_path, countries, languages } from './constants'
import {
    // addUserToOnline,
    setOnlineUsers,
    login,
    activeLocalSrc,
    activePeerSrc,
    initLocalSrc,
    initPeerSrc,
    incomingCall,
    calling,
    establishedCall,
    endCall,
    rejectCall,
    enterRoom,
    exitRoom
} from './redux/actions'

import PeerConnection from './PeerConnection'
import CallWindow from './CallWindow'
import CallModal from './CallModal'
import socket from './socket'
import './App.scss'

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />
const checkedIcon = <CheckBoxIcon fontSize="small" />

const useStyles = makeStyles(theme => ({
    app: {
        flex: 1,
        marginTop: 100
    },
    login: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    txtClientName: {
        color: 'white',
        marginLeft: 20,
        fontSize: 15,
        height: 40,
        border: 'none',
        borderBottomColor: 'white',
        borderBottom: 'solid 1px',
        backgroundColor: 'transparent',
    },
    imageUploader: {
        width: '30%',
        backgroundColor: 'transparent',
    },
    option: {
        fontSize: 15,
        '& > span': {
            marginRight: 10,
            fontSize: 18,
            color: 'white',
        },
    },
    country: {
        width: 300,
        color: 'white',
        marginTop: theme.spacing(2),
    },
    countryText: {
        root: {
            '& input:valid + fieldset': {
                borderColor: 'green',
                borderWidth: 2,
            },
        }
    },
    fab: {
        margin: theme.spacing(1),
    },
    close: {
        padding: theme.spacing(0.5),
    },
    chatRoom: {
        width: '80%',
        height: 800,
        overflow: 'auto',
        overflowX: 'hidden',
        border: "1px solid #ddd",
    },
    captionStyle: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: "rgba(0, 0, 0, 0.8)",
        height: 50,
        overflow: "hidden",
        position: "absolute",
        bottom: "0",
        width: "100%",
        color: "white",
        padding: "2px",
        fontSize: "90%"    
    },
    callButton: {
        pointerEvents: 'auto',
        cursor: 'pointer',
        color: 'green'
    },
    sidebar: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        overflowY: 'auto',
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 10,
    },
    modal: {
        display: 'flex',
        padding: theme.spacing(1),
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        width: 400,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    rooms: {
        // flex: 1,
        flexDirection: 'column',
        backgroundColor: '#4791db60'
    }
}));

const CssTextField = withStyles({
    root: {
        '& input': {
            color: 'white',
        },
        '& button': {
            color: 'white',
        },
        '& label': {
            color: 'white',
        },
        '& label.Mui-focused': {
            color: 'white',
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: 'white',
                color: 'white'
            },
            // '&:hover fieldset': {
            //   borderColor: 'white'
            // },
            '&.Mui-focused fieldset': {
                borderColor: 'white'
            },
        },
    },
})(TextField);

function countryToFlag(isoCode) {
    return typeof String.fromCodePoint !== 'undefined'
        ? isoCode.toUpperCase().replace(/./g, char => String.fromCodePoint(char.charCodeAt(0) + 127397))
        : isoCode;
}

function App() {
    const classes = useStyles()
    const [blogin, setBlogin] = useState(false)
    const [postImage, setPostImage] = useState(null)
    const [snack, setSnack] = useState({ open: false, message: '' })
    const [country, setCountry] = useState(null)
    const [language, setLanguage] = useState([])
    const userID = useRef('')
    const [clientName, setClientName] = useState('')
    const [callModal, setCallModal] = useState({ callModal: '', callFrom: '' })
    const [callWindow, setCallWindow] = useState('')
    const [localSrc, setLocalSrc] = useState(null)
    const [peerSrc, setPeerSrc] = useState(null)
    const [selectClient, setSelectClient] = useState(null)
    const [openRoomModal, setopenRoomModal] = useState(false)
    const [roomName, setroomName] = useState('')
    const [currentRoom, setcurrentRoom] = useState(null)
    const onlineUsers = useSelector(state=>state.onlineCilents)
    const rooms = useSelector(state=>state.Rooms)

    const dispatch = useDispatch()
    const pc = useRef({})
    const conf = useRef(null)

    const startCallHandler = (isCaller, friendID, config)=>{
        conf.current = config;
        pc.current = new PeerConnection(friendID)
          .on('localStream', (src) => {
            setCallWindow('active')
            dispatch(calling(friendID))
            setLocalSrc(src)
            dispatch(activeLocalSrc())
            if (!isCaller) {
                setCallModal({
                    callModal: '',
                    callFrom: ''
                })
            }
          })
          .on('peerStream', src => {
            dispatch(activePeerSrc())
            setPeerSrc(src)
          })
          .start(isCaller, config);
      }
    
    const endCallHandler = isStarter => {
        if (!isStarter) {
            dispatch(rejectCall())
        } else {
            dispatch(endCall())
        }
        if (_.isFunction(pc.current.stop)) {
            pc.current.stop(isStarter);
        }
        pc.current = {};
        conf.current = null;
        setCallWindow('')
        setCallModal({callModal:'', callFrom: ''})
        setLocalSrc(null)
        dispatch(initLocalSrc())
        setPeerSrc(null)
        dispatch(initPeerSrc())
    }

    const rejectCallHandler = ()=>{
        socket.emit('end', { to: callModal.callFrom })
        setCallModal({
            callModal: '',
            callFrom: ''
        })
    }
    
    const getonlineusers = () => {
        axios.post(server_url + '/' + getonlineusers_path, {
            clientId: userID.current
        })
        .then((res) => {
            const onlineUsers = res.data.onlineUsers
            const onlineRooms = res.data.onlineRooms
            dispatch(setOnlineUsers(onlineUsers, onlineRooms))
        }).catch((error) => {
            console.log(error.message)
        });

    }
    useEffect(() => {
        if (blogin && clientName) {
            const intervalID = setInterval(()=>{
                getonlineusers()
            }, 10000)
            return () => {
                clearInterval(intervalID)
            }
        }
    }, [blogin])

    const enterVideoCall = () => {
        if (!clientName) {
            setSnack({ open: true, message: 'Please enter your name!' })
            return

        }
        if (postImage == null) {
            setSnack({ open: true, message: 'Please choose a posting image!' })
            return
        }
        if (country == null) {
            setSnack({ open: true, message: 'Please choose a country!' })
            return
        }
        if (language.length == 0) {
            setSnack({ open: true, message: 'Please choose a language!' })
            return
        }
        socket.on('init', ({ id: cid }) => {
            document.title = `${clientName} - VideoCall`
            userID.current = cid
            const formData = new FormData()
            formData.append('postImage', postImage)
            formData.set('clientId', userID.current)
            formData.set('clientName', clientName)
            formData.set('country', country)
            formData.set('language', language)
            axios.post(server_url + '/' + enter_path, formData, {headers: {
                'content-type': 'multipart/form-data'
            }}).then((res) => {
                dispatch(login(clientName, postImage, country, language))
                setBlogin(true)
            }).catch((error) => {
                console.log(error.message)
            });
        }).on('request', ({ from: callFrom }) => {
            setCallModal({ callModal: 'active', callFrom })
            dispatch(incomingCall(callFrom))
        }).on('call', (data) => {
            if (data.sdp) {
                pc.current.setRemoteDescription(data.sdp);
                if (data.sdp.type === 'offer') {
                    pc.current.createAnswer()
                }
            } else {
                pc.current.addIceCandidate(data.candidate)
            }
        }).on('end', ()=>endCallHandler(false)).emit('init');
    }

    const handleSnack = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnack(false);
    }

    const handlePostingImage = (pictureFiles, pictureDataURLs) => {
        setPostImage(pictureFiles[0])
    }

    const handleCountry = (_, country) => {
        setCountry(country.label)
    }

    const handleLanguage = (_, language) => {
        setLanguage(language[0].title)
    }

    const onCurrentImageChange = (index)=>{
        setSelectClient(index)
    }
    const ChattingRoom = ({onlineUsers}) => {
        const IMAGES = onlineUsers.map((userInfo, index)=>{
            // console.log('label:', userInfo.country)
            // console.log('title:', userInfo.language[0])
            const tags = [{value: 'name:'+userInfo.clientName, title: 'name:'+userInfo.clientName}, {value: userInfo.country, title: userInfo.country}, {value: userInfo.language, title: userInfo.language}]

            return {
                index,
                src: server_url+'/'+userInfo.postImage.src,
                thumbnail: server_url+'/'+userInfo.postImage.src,
                thumbnailWidth: userInfo.postImage.width,
                thumbnailHeight: userInfo.postImage.height,
                caption: userInfo.clientName,
                tags,
                customOverlay:
                userInfo.clientId == userID.current
                ?
                null
                :
                    <div className={classes.captionStyle}>
                        <p>{userInfo.clientName}</p>
                        <FontAwesomeIcon icon={faVideo} size="2x" className={classes.callButton}
                            onClick={()=>{
                                startCallHandler(true, userInfo.clientId, {audio: true, video: true})
                            }}
                        />
                        <FontAwesomeIcon icon={faPhone} size="2x" className={classes.callButton}
                            onClick={()=>{
                                startCallHandler(true, userInfo.clientId, {audio: true, video: false})
                            }}
                        />
                    </div>,
            }
        })
        // console.log(IMAGES)
        return (
            <Gallery
                images={IMAGES}
                enableImageSelection={false}
                currentImageWillChange={onCurrentImageChange}
                customControls={
                [
                <FontAwesomeIcon icon={faVideo}  key='video' size="2x" className={classes.callButton}
                    onClick={()=>{
                        if (onlineUsers[selectClient].clientId == userID.current) {
                            return
                        }
                        startCallHandler(true, onlineUsers[selectClient].clientId, {audio: true, video: true})
                    }}
                />,
                <FontAwesomeIcon icon={faPhone} key='audio' size="2x" className={classes.callButton}
                    onClick={()=>{
                        if (onlineUsers[selectClient].clientId == userID.current) {
                            return
                        }
                        startCallHandler(true, onlineUsers[selectClient].clientId, {audio: true, video: false})
                    }}
                />
                ]
            }
            // onClickThumbnail={()=>{

            // }}
            />
        )
    }

    const RoomList = ({currentRoom, activeRoom, rooms, enterRoom}) => {
        return (
            <List>
              <ListItem button selected={currentRoom?false:true}
                onClick={()=>activeRoom(null)}
                key='allusers'
              >
                <ListItemText primary="All users"/>
              </ListItem>
              {
                  rooms.map(item=>{
                      return (
                        <ListItem button
                            onClick={()=>activeRoom(item)}
                            selected={currentRoom&&currentRoom.id==item.id?true:false}
                            key={item.id}
                        >
                            <ListItemAvatar>
                                <Avatar src={server_url+'/'+item.postImage.src}/>
                            </ListItemAvatar>
                            <ListItemText primary={item.roomName} id={item.id}/>
                            <ListItemSecondaryAction>
                            <Checkbox
                                edge="end"
                                onChange={e=>enterRoom(item, e.target.checked)}
                                checked={item.clientId==userID.current||item.users.includes(userID.current)?true:false}
                                inputProps={{ 'aria-labelledby': item.id }}
                            />
                            </ListItemSecondaryAction>
                        </ListItem>
                        )
                  })
              }
            </List>
          )
    }
    const handleClientName = e => {
        setClientName(e.target.value)
    }
    const handleChangeRoomName = e => {
        setroomName(e.target.value)
    }
    const handleCreateRoom = () => {
        setopenRoomModal(false)
        if (roomName) {
            axios.post(server_url + '/' + createroom_path, {
                clientId: userID.current,
                roomName
            }).then((res) => {
                // const onlineUsers = res.data.onlineUsers
                // const rooms = res.data.rooms
                // dispatch(setOnlineUsers({onlineUsers, rooms}))
            }).catch((error) => {
                console.log(error.message)
            });
        } else {
            setSnack({ open: true, message: 'Please enter room name!' })
            return
        }
    }
    const handleActiveRoom = item => {
        if (item) {
            setcurrentRoom(item)
        } else {
            setcurrentRoom(null)
        }
    }
    const roomUsers = currentRoom?(onlineUsers.filter(item=>{
        if (item.clientId == userID.current) {
            return false
        }
        if (item.clientId == currentRoom.clientId) {
            return true
        }
        if (currentRoom.users.includes(item.clientId)) {
            return true
        }
        return false
    })):onlineUsers
    const handleEnterRoom = (room, checked)=>{
        axios.post(server_url + '/' + enterroom_path, {
            clientId: userID.current,
            id: room.id,
            checked
        }).then((res) => {
            // const onlineUsers = res.data.onlineUsers
            // const rooms = res.data.rooms
            // dispatch(setOnlineUsers({onlineUsers, rooms}))
            if (checked) {
                dispatch(enterRoom(room.roomName))
            } else {
                dispatch(exitRoom(room.roomName))
            }
            getonlineusers()
        }).catch((error) => {
            console.log(error.message)
        });
    }
    return (
        <Sidebar
            sidebar={
                <div className={classes.sidebar}>
                    <span className="navbar-brand">
                        <img src={require('./icon1.png')} /> TUSI Marketplace
                    </span>
                    <Divider/>
                    {
                        blogin &&
                        <Button
                            variant="contained"
                            color="secondary"
                            startIcon={<AddIcon />}
                            onClick={()=>setopenRoomModal(true)}
                        >
                            Create room
                        </Button>
                    }
                    <Divider/>
                    {
                        blogin &&
                        <div className={classes.rooms}>
                            <RoomList
                                currentRoom={currentRoom}
                                activeRoom={handleActiveRoom}
                                rooms={rooms}
                                enterRoom={handleEnterRoom}
                            />
                        </div>
                    }
                </div>
            }
            docked={true}
        >
        <div className={classes.app}>
            {
                blogin
                    ?
                    <div className={classes.chatRoom}>
                        <ChattingRoom onlineUsers={roomUsers}/>
                        {conf.current &&
                        <CallWindow
                            status={callWindow}
                            localSrc={localSrc}
                            peerSrc={peerSrc}
                            config={conf.current}
                            mediaDevice={pc.current.mediaDevice}
                            endCall={endCallHandler}
                        />
                        }
                        <CallModal
                            status={callModal.callModal}
                            startCall={startCallHandler}
                            rejectCall={rejectCallHandler}
                            callFrom={callModal.callFrom}
                        />
                    </div>
                    :
                    <div className={classes.login}>
                        <h2>
                            Enter your name
                            <input
                                type="text"
                                className={classes.txtClientName}
                                value={clientName}
                                onChange={handleClientName}
                            />
                        </h2>
                        <ImageUploader
                            className={classes.imageUploader}
                            buttonText='Choose a image'
                            withLabel={false}
                            label={'Upload your posting image'}
                            withIcon={true}
                            withPreview={true}
                            singleImage={true}
                            onChange={handlePostingImage}
                            imgExtension={[".jpg", ".png"]}
                        />
                        <Autocomplete
                            options={countries}
                            classes={{
                                root: classes.country,
                                option: classes.option,
                            }}
                            autoHightlight
                            getOptionLabel={option => option.label}
                            renderOption={option => (
                                <React.Fragment>
                                    <span>{countryToFlag(option.code)}</span>
                                    {option.label} ({option.code}) +{option.phone}
                                </React.Fragment>
                            )}
                            renderInput={params => (
                                <CssTextField
                                    {...params}
                                    label="Choose a country"
                                    variant="outlined"
                                    fullWidth
                                    inputProps={{
                                        ...params.inputProps,
                                        autoComplete: 'disabled', // disable autocomplete and autofill
                                    }}
                                />
                            )}
                            onChange={handleCountry}
                        />
                        <Autocomplete
                            multiple
                            options={languages}
                            classes={{
                                root: classes.country,
                                option: classes.option,
                            }}
                            disableCloseOnSelect
                            getOptionLabel={option => option.title}
                            renderOption={(option, { selected }) => (
                                <React.Fragment>
                                    <Checkbox
                                        icon={icon}
                                        checkedIcon={checkedIcon}
                                        style={{ marginRight: 8 }}
                                        checked={selected}
                                    />
                                    {option.title}
                                </React.Fragment>
                            )}
                            renderInput={params => (
                                <CssTextField
                                    {...params}
                                    variant="outlined"
                                    label="Choose languages"
                                    placeholder="Favorites"
                                    fullWidth
                                />
                            )}
                            onChange={handleLanguage}
                        />
                        <Fab color="secondary" aria-label="edit" className={classes.fab}>
                            <TrendingFlat onClick={enterVideoCall} />
                        </Fab>
                    </div>

            }
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                open={snack.open}
                autoHideDuration={2000}
                onClose={handleSnack}
                ContentProps={{
                    'aria-describedby': 'message-id',
                }}
                message={<span id="message-id">{snack.message}</span>}
                action={[
                    // <Button key="undo" color="secondary" size="small" onClick={handleSnack}>
                    //   UNDO
                    // </Button>,
                    <IconButton
                        key="close"
                        aria-label="close"
                        color="inherit"
                        className={classes.close}
                        onClick={handleSnack}
                    >
                        <CloseIcon />
                    </IconButton>,
                ]}
            />
            <Modal
                aria-labelledby="spring-modal-title"
                aria-describedby="spring-modal-description"
                open={openRoomModal}
                className={classes.modal}
                onClose={()=>setopenRoomModal(false)}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <div className={classes.paper}>
                    <TextField id="standard-basic" label="Room name"
                        onChange={handleChangeRoomName}
                    />
                    <Button
                        variant="contained"
                        color="secondary"
                        onClick={handleCreateRoom}
                    >
                        Create room
                    </Button>
                </div>
            </Modal>
            </div>
        </Sidebar>
    );
}

export default App;
