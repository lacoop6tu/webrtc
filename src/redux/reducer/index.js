import { combineReducers } from 'redux'
import onlineCilents from './onlineUsers'
import Rooms from './Rooms'

export default combineReducers({
    onlineCilents,
    Rooms
})
